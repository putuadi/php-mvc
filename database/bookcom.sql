-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 30 Okt 2023 pada 06.57
-- Versi server: 8.0.30
-- Versi PHP: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bookcom`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `buku`
--

CREATE TABLE `buku` (
  `id` int NOT NULL,
  `gambar` text NOT NULL,
  `judul` text NOT NULL,
  `deskripsi` text NOT NULL,
  `harga` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data untuk tabel `buku`
--

INSERT INTO `buku` (`id`, `gambar`, `judul`, `deskripsi`, `harga`) VALUES
(1, '650821ea41ff2.jpg', 'Gerunk Junior', 'Kisah 6 orang sukses dari hooki group. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt natus fugiat neque rem totam velit repellat debitis. Libero numquam placeat expedita quaerat laboriosam. Officia rem eius nihil sunt debitis excepturi optio veniam fuga explicabo, sint iure pariatur, soluta nostrum atque doloribus earum? Fuga obcaecati tempora eius architecto perferendis porro, ut ex accusamus. Nihil dicta neque, molestiae, inventore vero, pariatur animi voluptates ab quod voluptatem ad assumenda aliquam iste officiis! Soluta rem blanditiis maiores in eius dolor modi quo provident velit.', 100000),
(11, '6518dff725c6c.png', 'Ruang Sunyi', 'Sinopsis : Kisah Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt natus fugiat neque rem totam velit repellat debitis. Libero numquam placeat expedita quaerat laboriosam. Officia rem eius nihil sunt debitis excepturi optio veniam fuga explicabo, sint iure pariatur, soluta nostrum atque doloribus earum? Fuga obcaecati tempora eius architecto perferendis porro, ut ex accusamus. Nihil dicta neque, molestiae, inventore vero, pariatur animi voluptates ab quod voluptatem ad assumenda aliquam iste officiis! Soluta rem blanditiis maiores in eius dolor modi quo provident velit.', 60000),
(12, '6518e4015dbab.png', 'Kisah Tanah Jawa', 'Sinopsis : Kisah Tanah Jawa. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt natus fugiat neque rem totam velit repellat debitis. Libero numquam placeat expedita quaerat laboriosam. Officia rem eius nihil sunt debitis excepturi optio veniam fuga explicabo, sint iure pariatur, soluta nostrum atque doloribus earum? Fuga obcaecati tempora eius architecto perferendis porro, ut ex accusamus. Nihil dicta neque, molestiae, inventore vero, pariatur animi voluptates ab quod voluptatem ad assumenda aliquam iste officiis! Soluta rem blanditiis maiores in eius dolor modi quo provident velit.', 90000),
(13, '6518ecf41699f.png', 'Fantasteen', 'Sinopsis : Kisah Fantasteen Lorem ipsum dolor sit amet, consectetur adipisicing. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt natus fugiat neque rem totam velit repellat debitis. Libero numquam placeat expedita quaerat laboriosam. Officia rem eius nihil sunt debitis excepturi optio veniam fuga explicabo, sint iure pariatur, soluta nostrum atque doloribus earum? Fuga obcaecati tempora eius architecto perferendis porro, ut ex accusamus. Nihil dicta neque, molestiae, inventore vero, pariatur animi voluptates ab quod voluptatem ad assumenda aliquam iste officiis! Soluta rem blanditiis maiores in eius dolor modi quo provident velit.', 120);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `role` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `role`) VALUES
(2, 'adi@gmail.com', '$2y$10$0y7hGtui7cT6ktOy68gJFevuZzvcOCe4RqN4QMGmsTi6Ar2NUdH6m', 1),
(3, 'edmund@gmail.com', '$2y$10$t4rAvrmRd7u5Z7vNwySbxuDNCjLISHhUEmTrMeFAGFAeFn9ijxm8a', 0),
(4, 'nik@gmail.com', '$2y$10$w5YuzdgzTvr.G6IoDB32GOKxW07cneWNXGEwrrcMJf2EZ6Y.GOKue', 0),
(5, 'angga@gmail.com', '$2y$10$.NkP2Mk0Ivfvoc/vt367N.fri3bcFqyY22b4K68yqVOl9WVz82hKS', 0);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `buku`
--
ALTER TABLE `buku`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `buku`
--
ALTER TABLE `buku`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
