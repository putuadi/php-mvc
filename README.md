# PHP MVC

PHP MVC yang telah dikembangkan adalah sebuah platform penjualan buku online yang saat ini sedang dalam tahap pengembangan. Saat ini, platform hanya menyediakan fitur Register, Login, dan dasar CRUD untuk pengelolaan buku.

Selanjutnya, beberapa fitur yang akan segera ditambahkan ke dalam platform ini antara lain:

- Daftar pembelian pada halaman admin.
- Keranjang pembelian pada halaman user.
- Dan fitur lainnya yang akan ditentukan.

**Login sebagai admin:**
- Username: adi@gmail.com
- Password: adi

**Login sebagai user:**
- Username: edmund@gmail.com
- Password: edmund

