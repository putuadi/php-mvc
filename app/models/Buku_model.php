<?php

class Buku_model
{
    private $table = "buku";
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }
    public function getAllBuku()
    {
        $this->db->query('SELECT * FROM ' . $this->table);
        return $this->db->resultSet();
    }
    public function getDetailBuku($id)
    {
        $this->db->query('SELECT * FROM ' . $this->table . ' WHERE id=' . $id);
        return $this->db->single();
    }
    public function storeBuku($data, $file)
    {
        $nama_gambar = $file["gambar"]["name"];
        $tmp_name = $file["gambar"]["tmp_name"];

        $tipe_gambar = explode(".", $nama_gambar);
        $tipe_gambar = end($tipe_gambar);

        $nama_gambar = uniqid();
        $nama_gambar .= ".";
        $nama_gambar .= $tipe_gambar;

        move_uploaded_file($tmp_name, "../public/img/" . $nama_gambar);

        $this->db->query('INSERT INTO ' . $this->table . '(gambar,judul,deskripsi,harga) VALUES (:gambar,:judul,:deskripsi,:harga)');
        $this->db->bind('gambar', $nama_gambar);
        $this->db->bind('judul', $data['judul']);
        $this->db->bind('deskripsi', $data['deskripsi']);
        $this->db->bind('harga', $data['harga']);

        return $this->db->execute();
    }
    public function updateBuku($id, $data, $file)
    {
        if ($file["gambar"]["error"] != 4) {
            $nama_gambar = $file["gambar"]["name"];
            $tmp_name = $file["gambar"]["tmp_name"];

            $tipe_gambar = explode(".", $nama_gambar);
            $tipe_gambar = end($tipe_gambar);

            $nama_gambar = uniqid();
            $nama_gambar .= ".";
            $nama_gambar .= $tipe_gambar;

            move_uploaded_file($tmp_name, "../public/img/" . $nama_gambar);
            $this->db->query('UPDATE ' . $this->table . ' SET gambar = :gambar, judul = :judul, deskripsi = :deskripsi, harga = :harga WHERE id=' . $id);
            $this->db->bind('gambar', $nama_gambar);
            $this->db->bind('judul', $data['judul']);
            $this->db->bind('deskripsi', $data['deskripsi']);
            $this->db->bind('harga', $data['harga']);
            return $this->db->execute();
        }

        $this->db->query('UPDATE ' . $this->table . ' SET judul = :judul, deskripsi = :deskripsi, harga = :harga WHERE id=' . $id);
        $this->db->bind('judul', $data['judul']);
        $this->db->bind('deskripsi', $data['deskripsi']);
        $this->db->bind('harga', $data['harga']);

        return $this->db->execute();
    }
    public function deleteBuku($id)
    {
        $this->db->query('DELETE FROM ' . $this->table . ' WHERE id=' . $id);
        return $this->db->execute();

    }
}
