<?php

class Auth_model
{
    private $table = "user";
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }
    public function registerUser($data)
    {
        $this->db->query('INSERT INTO ' . $this->table . '(username, password, role) VALUES (:username,:password, 0)');
        $this->db->bind('username', $data['username']);
        $this->db->bind('password', password_hash($data['password'], PASSWORD_DEFAULT));

        return $this->db->execute();
    }
    public function getUser($data)
    {
        $this->db->query('SELECT * FROM ' . $this->table . ' WHERE username=' . '"' . $data["username"] . '"');
        return $this->db->single();
    }
    public function loginUser($data, $data_user)
    {
        if (password_verify($data["password"], $data_user["password"])) {
            return true;
        }

        return false;
    }
}
