<?php
class Buku extends Controller
{
    public function __construct()
    {
        if (!isset($_SESSION["user"]) || $_SESSION["user"]["role"] != 1) {
            echo "
                <script>
                    window.location.href='" . BASE_URL . "status/forbidden';
                </script>
            ";
        }
    }
    public function index()
    {
        $data = [
            "title" => "Buku",
            "buku" => $this->model('Buku_model')->getAllBuku(),
        ];
        $this->view('templates/header', $data);
        $this->view('buku/index', $data);
        $this->view('templates/footer');
    }
    public function detail($id)
    {
        $data = [
            "title" => "Detail",
            "buku" => $this->model('Buku_model')->getDetailBuku($id),
        ];
        $this->view('templates/header', $data);
        $this->view('buku/detail', $data);
        $this->view('templates/footer');
    }
    public function tambah()
    {
        $data = [
            "title" => "Tambah",
        ];
        $this->view('templates/header', $data);
        $this->view('buku/tambah');
        $this->view('templates/footer');
    }
    public function edit($id)
    {
        $data = [
            "title" => "Edit",
            "buku" => $this->model('Buku_model')->getDetailBuku($id),
        ];
        $this->view('templates/header', $data);
        $this->view('buku/edit', $data);
        $this->view('templates/footer');
    }
    public function store()
    {
        if ($this->model('Buku_model')->storeBuku($_POST, $_FILES)) {
            echo "
                <script>
                    alert('Data berhasil ditambahkan');
                    window.location.href='" . BASE_URL . "buku/';
                </script>
            ";
            exit;
        }
    }
    public function update($id)
    {
        if ($this->model('Buku_model')->updateBuku($id, $_POST, $_FILES)) {
            echo "
                <script>
                    alert('Data berhasil diedit');
                    window.location.href='" . BASE_URL . "buku/';
                </script>
            ";
            exit;
        }
    }
    public function delete($id)
    {
        if ($this->model('Buku_model')->deleteBuku($id)) {
            echo "
                <script>
                    alert('Data berhasil dihapus');
                    window.location.href='" . BASE_URL . "buku/';
                </script>
            ";
            exit;
        }
    }
}
