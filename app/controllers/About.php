<?php

class About extends Controller
{
    public function index($nama = "Adi", $pekerjaan = "LKSN")
    {
        $data = [
            "title" => "About",
            "nama" => $nama,
            "pekerjaan" => "Pejuang " . $pekerjaan,
        ];
        $this->view('templates/header', $data);
        $this->view('about/index', $data);
        $this->view('templates/footer');
    }
    public function page()
    {
        $data = [
            "title" => "About Page",
        ];
        $this->view('templates/header', $data);
        $this->view('about/page');
        $this->view('templates/footer');
    }
}
