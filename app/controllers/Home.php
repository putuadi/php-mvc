<?php

class Home extends Controller
{
    public function index()
    {
        $user = $this->model('User_model')->getUser();
        $data = [
            "title" => "Home",
            "user" => $user,
            "buku" => $this->model('Buku_model')->getAllBuku(),
        ];
        $this->view('templates/header', $data);
        $this->view('home/index', $data);
        $this->view('templates/footer');
    }
}
