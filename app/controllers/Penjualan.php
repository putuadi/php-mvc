<?php

class Penjualan extends Controller
{

    public function __construct()
    {
        if (!isset($_SESSION["user"]) || $_SESSION["user"]["role"] != 1) {
            echo "
                <script>
                    window.location.href='" . BASE_URL . "status/forbidden';
                </script>
            ";
        }
    }
    public function index()
    {
        $data = [
            "title" => "Penjualan",
        ];
        $this->view('templates/header', $data);
        $this->view('penjualan/index', $data);
        $this->view('templates/footer');
    }
}
