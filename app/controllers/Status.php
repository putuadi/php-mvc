<?php

class Status extends Controller
{
    public function forbidden()
    {
        $data = [
            "title" => "Forbidden",
        ];
        $this->view('templates/header', $data);
        $this->view('home/forbidden');
        $this->view('templates/footer');
    }
}
