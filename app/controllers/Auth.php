<?php
class Auth extends Controller
{
    public function halaman_login()
    {
        $data = [
            "title" => "Login",
        ];
        $this->view('templates/header', $data);
        $this->view('auth/login', $data);
        $this->view('templates/footer');
    }
    public function halaman_register()
    {
        $data = [
            "title" => "Register",
        ];
        $this->view('templates/header', $data);
        $this->view('auth/register', $data);
        $this->view('templates/footer');
    }
    public function register()
    {
        if ($this->model('Auth_model')->registerUser($_POST)) {
            echo "
                <script>
                    alert('Berhasil registrasi');
                    window.location.href='" . BASE_URL . "auth/halaman_login';
                </script>
            ";
            exit;
        }
    }
    public function login()
    {
        $user = $this->model('Auth_model')->getUser($_POST);
        if ($user && $this->model('Auth_model')->loginUser($_POST, $user)) {
            $_SESSION["user"] = $user;
            echo "
                    <script>
                        window.location.href='" . BASE_URL . "';
                    </script>
                ";
            exit;
        }
        echo "
            <script>
                alert('Gagal login');
                window.location.href='" . BASE_URL . "auth/halaman_login';
            </script>
        ";
    }
    public function logout()
    {
        session_destroy();
        echo "
            <script>
                window.location.href='" . BASE_URL . "auth/halaman_login';
            </script>
        ";
    }
}
