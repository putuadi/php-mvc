<h1>Detail Buku</h1>
<hr>

<div class="d-flex flex-wrap mx-auto w-100 justify-content-center">
    <img src="<?=BASE_URL?>img/<?=$data['buku']['gambar']?>" alt="" height="300px" class="rounded border shadow my-2" style="border-color: #efefef;">
    <div class="w-50 text-start ms-3 my-2 ps-3 border-start">
        <h4 style="font-weight: normal;">Judul : <?=$data['buku']['judul']?></h4>
        <p>Sinopsis : <?=$data['buku']['deskripsi']?></p>
        <h5 class="text-primary" style="font-weight: normal;">Harga : <?=$data['buku']['harga']?></h5>
    </div>
</div>

