<h1>Tambah Buku</h1>
<hr>
<form action="<?=BASE_URL?>buku/store" method="POST" class="text-start" enctype="multipart/form-data">
<div class="mb-3">
    <label for="gambar" class="form-label">Gambar</label>
    <input class="form-control" type="file" id="gambar" name="gambar" accept="Image/*" required>
    <img src="" alt="" style="height:200px;" class="mt-2 rounded border" style="border-color: #efefef;">
</div>
  <div class="mb-3">
    <label for="judul" class="form-label">Judul</label>
    <input type="text" class="form-control" id="judul" name="judul" required>
  </div>
  <div class="mb-3">
    <label for="deskripsi" class="form-label">Deskripsi</label>
    <input type="text" class="form-control" id="deskripsi" name="deskripsi" required>
  </div>
  <div class="mb-3">
    <label for="harga" class="form-label">Harga</label>
    <input type="number" class="form-control" id="harga" name="harga" required>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
