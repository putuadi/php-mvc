<h1>Data Buku</h1>
<hr>
<div class="text-start position-fixed mx-5 my-3 shadow" style="right:0;bottom:0;">
<a href="<?=BASE_URL?>buku/tambah" class="btn btn-primary fs-3 px-3"><b>+</b></a>
</div>
<table class="table table-striped">
    <tr>
        <th scope="col">Gambar</th>
        <th scope="col">Judul</th>
        <th scope="col">Harga</th>
        <th scope="col">Aksi</th>
    </tr>
    <?php foreach ($data["buku"] as $data_buku): ?>
        <tr>
            <td scope="row"><img src="<?=BASE_URL?>img/<?=$data_buku['gambar']?>" style="height:200px;" class="rounded" alt=""></td>
            <td class="align-items-center"><?=$data_buku['judul']?></td>
            <td><?=$data_buku['harga']?></td>
            <td><a href="<?=BASE_URL?>buku/detail/<?=$data_buku['id']?>">Detail</a> | <a href="<?=BASE_URL?>buku/edit/<?=$data_buku['id']?>">Edit</a> | <a href="<?=BASE_URL?>buku/delete/<?=$data_buku['id']?>">Delete</a></td>
        </tr>
    <?php endforeach;?>
</table>