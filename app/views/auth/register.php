<h1>Register</h1>
<hr>
<form action="<?=BASE_URL?>auth/register" method="POST" class="text-start">
  <div class="mb-3">
    <label for="username" class="form-label">Username</label>
    <input type="email" class="form-control" id="username" name="username" required>
  </div>
  <div class="mb-3">
    <label for="password" class="form-label">Password</label>
    <input type="password" class="form-control" id="password" name="password" required>
  </div>
  <div class="d-flex justify-content-between">
  <button type="submit" class="btn btn-primary">Submit</button>
  <small>Punya akun? <a href="<?=BASE_URL?>auth/halaman_login">Login</a></small>
  </div>
</form>
