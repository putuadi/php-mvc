<h1>About</h1>
<hr>
<div class="d-flex flex-wrap mx-auto w-100 justify-content-center">
    <img src="<?=BASE_URL?>img/adisap.jpg" alt="" height="300px" class="rounded border shadow my-2" style="border-color: #efefef;">
    <div class="w-50 text-start ms-3 my-2 ps-3 border-start">
        <p>Selamat datang di website saya! Saya adalah I Putu Adi Saputra, seorang siswa yang berfokus belajar pada bidang Web Development. Dengan latar belakang pendidikan di SMKN 1 Denpasar, saya telah memperoleh pengetahuan dan keterampilan yang kuat dalam bidang Web Development terutama dalam bidang backend. Selama beberapa tahun terakhir, saya telah bekerja pada berbagai proyek yang menarik dan menantang. Saya telah berkontribusi dalam tim yang berhasil menghasilkan solusi kreatif dan inovatif. Saya juga memiliki rekam jejak prestasi dalam bidang Web Development, Anda bisa lihat sendiri pada halaman project.</p>
        <small>Aku <?=$data['nama']?> <?=$data['pekerjaan']?></small>
    </div>
</div>
<br>