
<h1>Buku</h1>
<!-- <p>Buku adalah cara unik manusia untuk memandang dunia. Buku menjelajahi semua bagian kehidupan, mengubah kehidupan, dan memungkinkan untuk melihat berbagai hal secara berbeda. Buku dapat mengubah hidupmu.</p> -->
<hr>
<div class="d-flex flex-wrap justify-content-center">
    <?php foreach ($data['buku'] as $data_buku): ?>
        <div class="card m-1" style="width: 18rem;">
            <img src="<?=BASE_URL?>img/<?=$data_buku['gambar']?>" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title"><?=$data_buku['judul']?></h5>
                <p class="home card-text limit-text"><?=$data_buku['deskripsi']?></p>
                <a href="#" class="btn btn-primary" onclick="alert('Fitur masih dalam pengembangan')">Beli buku</a>
            </div>
        </div>
    <?php endforeach;?>
</div>
