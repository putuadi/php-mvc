<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman <?=$data['title']?></title>

    <link href="<?=BASE_URL?>css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="<?=BASE_URL?>css/style.css">
</head>
<body>
<nav class="navbar navbar-expand-lg bg-primary">
  <div class="container-fluid d-flex justify-content-between px-5">
    <a class="navbar-brand" href="<?=BASE_URL?>">Book.com</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link <?=($data['title'] === 'Home') ? 'active' : ''?>" aria-current="page" href="<?=BASE_URL?>">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link <?=($data['title'] === 'About') ? 'active' : ''?>" href="<?=BASE_URL?>about/">About</a>
        </li>
        <?php if (isset($_SESSION["user"]) && $_SESSION["user"]["role"] == 1): ?>
          <li class="nav-item">
            <a class="nav-link <?=($data['title'] === 'Buku') ? 'active' : ''?>" href="<?=BASE_URL?>buku/">Buku</a>
          </li>
          <li class="nav-item">
            <a class="nav-link <?=($data['title'] === 'Penjualan') ? 'active' : ''?>" href="<?=BASE_URL?>penjualan/">Penjualan</a>
          </li>
        <?php endif;?>
        <?php if (!isset($_SESSION["user"])): ?>
          <li class="nav-item">
            <a class="nav-link <?=($data['title'] === 'Login') ? 'active' : ''?>" href="<?=BASE_URL?>auth/halaman_login">Login</a>
          </li>
          <li class="nav-item">
            <a class="nav-link <?=($data['title'] === 'Register') ? 'active' : ''?>" href="<?=BASE_URL?>auth/halaman_register">Register</a>
          </li>
        <?php endif;?>
        <?php if (isset($_SESSION["user"])): ?>
          <li class="nav-item">
            <a class="nav-link <?=($data['title'] === 'Logout') ? 'active' : ''?>" href="<?=BASE_URL?>auth/logout">Logout</a>
          </li>
        <?php endif;?>
        <!-- <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Dropdown
          </a>
          <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="#">Action</a></li>
            <li><a class="dropdown-item" href="#">Another action</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="#">Something else here</a></li>
          </ul>
        </li>
        <li class="nav-item">
          <a class="nav-link disabled" aria-disabled="true">Disabled</a>
        </li>
      </ul>
      <form class="d-flex" role="search">
        <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success" type="submit">Search</button>
      </form> -->
    </div>
  </div>
</nav>
<div class="container bg-white p-5 rounded-bottom shadow text-center">