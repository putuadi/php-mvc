
</div>
<script src="<?=BASE_URL?>js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
<script>
    const img = document.querySelector('img');
    const input = document.querySelector('#gambar');
    input.addEventListener('input', (e) => {
        img.src = URL.createObjectURL(e.target.files[0]);
    });
</script>
</body>
</html>